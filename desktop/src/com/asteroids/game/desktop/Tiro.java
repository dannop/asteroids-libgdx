package com.asteroids.game.desktop;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class Tiro extends SpaceController {


    public static final float COOLDOWN = 0.5f;



    public static float timer; //TODO: privado (?)
    private static Texture bala;

    private Position myPosition;

    public boolean destroy = false;

    Collision collision;

    public Tiro (float posX, float posY){
        super (posX, posY, 20, 20, 400);

        this.myPosition = new Position(posX, posY);
        Position mousePosition = new Position(Gdx.input.getX(), Gdx.input.getY());
        myPosition.setCatAdj(mousePosition);
        myPosition.setCatOpt(mousePosition);
        myPosition.setHip();
        this.collision = new Collision(posX, posY, width, height);

        if (bala == null){
            bala = new Texture("./core/assets/star.jpg");
        }
    }

    public void update (float deltaTime) {

        myPosition.x += (speed * -myPosition.getCos()) * deltaTime;
        myPosition.y += (speed * myPosition.getSen()) * deltaTime;


        if (myPosition.y > Gdx.graphics.getHeight() || myPosition.y < 0 || myPosition.x > Gdx.graphics.getWidth() || myPosition.x < 0){
            destroy = true;
        } 

        collision.move((float) myPosition.x, (float) myPosition.y);
    }

    public void render (SpriteBatch batch){
        batch.draw(bala, (float) myPosition.x, (float) myPosition.y, width, height);

    }

    public boolean isDestroy() {
        return destroy;
    }

    public Collision getCollision() {
        return collision;
    }
}
