package com.asteroids.game.desktop;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SuperInimigo extends SpaceObjects {

    public SuperInimigo(float x, float y, int flag) {
        super(x, y, flag, 5.0f, 0.2f, 100, "core/assets/200px-RamblinEvilMushroom.png");
        this.life = 2;
    }


    @Override
    public void render (SpriteBatch batch){ super.render(batch); }

    public int hit() {
        return this.damage += -30;
    }

}
