package com.asteroids.game.desktop;

import com.asteroids.game.Asteroids;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.Iterator;

public class GameOver implements Screen {


    public static final int WIDHT_IMG = 700;
    public static final int HEIGHT_IMG = 300;
    ArrayList <Integer> highs;
    Asteroids game;
    int score, highscore;
    Texture gameOverImg;
    BitmapFont scoreFont;

    public GameOver (Asteroids game, int score){
        this.game = game;
        this.score = score;
        this.highs = new ArrayList<Integer>();
        highs.add(score);
        Preferences preferences = Gdx.app.getPreferences("asteroids");
        this.highscore = preferences.getInteger("highscore", 0);
        highscore = highScore();
        preferences.getInteger("highscore", highscore);
        preferences.flush();


        gameOverImg =  new Texture("core/assets/game_over.png");
        scoreFont = new BitmapFont(Gdx.files.internal("core/assets/fonts/score.fnt"));
    }

    @Override
    public void show () {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();

        game.batch.draw(gameOverImg, Gdx.graphics.getWidth() / 2 - WIDHT_IMG / 2, Gdx.graphics.getHeight() - HEIGHT_IMG - 15, WIDHT_IMG, HEIGHT_IMG);

        GlyphLayout scoreLayout = new GlyphLayout(scoreFont, "Score: " + score, Color.WHITE, 0, Align.left, false);
        GlyphLayout highscoreLayout = new GlyphLayout(scoreFont, "Highscore: " + highscore, Color.WHITE, 0, Align.left, false);
        scoreFont.draw(game.batch, scoreLayout, Gdx.graphics.getWidth() / 2 - scoreLayout.width / 2, Gdx.graphics.getHeight() - HEIGHT_IMG - 15 * 2);
        scoreFont.draw(game.batch, highscoreLayout, Gdx.graphics.getWidth() / 2 - highscoreLayout.width / 2, Gdx.graphics.getHeight() - HEIGHT_IMG - scoreLayout.height - 15 * 3);

        GlyphLayout tryAgainLayout = new GlyphLayout(scoreFont, "Try Again");
        GlyphLayout menuLayout = new GlyphLayout(scoreFont, "Main Menu");

        float tryAgainX = Gdx.graphics.getWidth() / 2 - tryAgainLayout.width /2;
        float tryAgainY = Gdx.graphics.getHeight() / 2 - tryAgainLayout.height / 2 - 15 * 9;
        float mainMenuX = Gdx.graphics.getWidth() / 2 - menuLayout.width / 2;
        float mainMenuY = Gdx.graphics.getHeight() / 2 - menuLayout.height / 2 - tryAgainLayout.height - 15 * 10;

        float touchX = Gdx.input.getX();
        float touchY = Gdx.graphics.getHeight() - Gdx.input.getY();

        if (touchX >= tryAgainX && touchX < tryAgainX + tryAgainLayout.width && touchY   >= tryAgainY - tryAgainLayout.height && touchY < tryAgainY)
            tryAgainLayout.setText(scoreFont, "Try Again", Color.YELLOW, 0, Align.left, false);

        if (touchX >= mainMenuX && touchX < mainMenuX + menuLayout.width && touchY >= mainMenuY - menuLayout.height && touchY < mainMenuY)
            menuLayout.setText(scoreFont, "Main Menu", Color.YELLOW, 0, Align.left, false);

        if (Gdx.input.isTouched()) {
            if (touchX > tryAgainX && touchX < tryAgainX + tryAgainLayout.width && touchY > tryAgainY - tryAgainLayout.height && touchY < tryAgainY) {
                this.dispose();
                game.batch.end();
                game.setScreen(new GameController(game));
                return;
            }

            if (touchX > mainMenuX && touchX < mainMenuX + menuLayout.width && touchY > mainMenuY - menuLayout.height && touchY < mainMenuY) {
                this.dispose();
                game.batch.end();
                game.setScreen(new Menu(game));
                return;
            }
        }

        scoreFont.draw(game.batch, tryAgainLayout, tryAgainX, tryAgainY);
        scoreFont.draw(game.batch, menuLayout, mainMenuX, mainMenuY);

        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public int highScore(){

        int maior = 0;

        for (Integer score: highs) {
            if(maior <= score) {
                maior = score;
            }

        }
        return maior;
    }



}
