package com.asteroids.game.desktop;

import com.asteroids.game. Asteroids;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

public class Menu implements Screen {

    Asteroids game;

    private static final int EXIT_BUTTON_WIDTH = 250;
    private static final int EXIT_BUTTON_HEIGHT = 120;
    private static final int EXIT_BUTTON_Y = 200;
    private static final int PLAY_BUTTON_WIDTH = 250;
    private static final int PLAY_BUTTON_HEIGHT = 120;
    private static final int PLAY_BUTTON_Y = 400;

    Texture exitButtonActive;
    Texture exitButtonInactive;
    Texture playButtonActive;
    Texture playButtonInactive;

    public Menu(Asteroids game) {
        this.game = game;
        playButtonActive = new Texture("core/assets/play_button_active.png");
        playButtonInactive = new Texture("core/assets/play_button_inactive.png");
        exitButtonActive = new Texture("core/assets/exit_button_active.png");
        exitButtonInactive = new Texture("core/assets/exit_button_inactive.png");
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        //cor da tela
        Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
        Gdx.gl.glClear(16384);
        this.game.batch.begin();

        //coordenada do eixo x para centralizar o botao: tamanho da dela/2 - tamanho do botao/2
        int xPosition = (Asteroids.WIDTH_SCREEN / 2) - (EXIT_BUTTON_WIDTH / 2);

        //pega a coordenada do mouse e confere se ela está sobre a coordenada do botao
        if ((Gdx.input.getX() < xPosition +  EXIT_BUTTON_WIDTH) && (Gdx.input.getX() > xPosition) && (Asteroids.HEIGHT_SCREEN - Gdx.input.getY() < EXIT_BUTTON_Y + EXIT_BUTTON_HEIGHT) && (Asteroids.HEIGHT_SCREEN - Gdx.input.getY() > EXIT_BUTTON_Y)){
            game.batch.draw (exitButtonActive, xPosition, EXIT_BUTTON_Y, EXIT_BUTTON_WIDTH, EXIT_BUTTON_HEIGHT);
            //se receber o clique, fecha o jogo
            if(Gdx.input.isTouched()) {
                Gdx.app.exit();
            }
        } else game.batch.draw (exitButtonInactive, xPosition, EXIT_BUTTON_Y, EXIT_BUTTON_WIDTH, EXIT_BUTTON_HEIGHT);


        xPosition = (Asteroids.WIDTH_SCREEN / 2) - (PLAY_BUTTON_WIDTH / 2);

        if ((Gdx.input.getX() < xPosition +  PLAY_BUTTON_WIDTH) && (Gdx.input.getX() > xPosition) && (Asteroids.HEIGHT_SCREEN - Gdx.input.getY() < PLAY_BUTTON_Y + PLAY_BUTTON_HEIGHT) && (Asteroids.HEIGHT_SCREEN - Gdx.input.getY() > PLAY_BUTTON_Y)){
            game.batch.draw (playButtonActive, xPosition, PLAY_BUTTON_Y, PLAY_BUTTON_WIDTH, PLAY_BUTTON_HEIGHT);
            if (Gdx.input.isTouched()) {
                this.dispose();
                game.setScreen(new GameController(game));
            }
        } else {
            game.batch.draw (playButtonInactive, xPosition, PLAY_BUTTON_Y, PLAY_BUTTON_WIDTH, PLAY_BUTTON_HEIGHT);
        }

        this.game.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
