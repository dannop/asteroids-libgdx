package com.asteroids.game.desktop;

import com.asteroids.game.Asteroids;
import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;



import java.util.ArrayList;
import java.util.Random;

public class GameController implements Screen {

    Random random;
    public static final float SPEED = 250; //Velocidade de frame no jogo.

    public Asteroids game;
    public Jogador player;
    public SpawnController spawn;


    //Inimigos

    //Explosões
    //public ArrayList<Explosion> explosions;
    public static float timer;

    //Score
    BitmapFont scoreFont;

    //Vidas
    Texture lifeBar;
    Collision playerCollision;
    //Medida temporária para funcionamento da barra de vidas
    public static final int SHIP_WIDTH_PIXEL = 17;
    public static final int SHIP_HEIGHT_PIXEL = 32;
    public static final int SHIP_WIDTH = SHIP_WIDTH_PIXEL * 3;
    public static final int SHIP_HEIGHT = SHIP_HEIGHT_PIXEL * 3;


    @Override
    public void show() {
        player = new Jogador();


    }

    public GameController(Asteroids game){
        this.game = game;
        this.random = new Random();
        spawn = new SpawnController();
        scoreFont = new BitmapFont(Gdx.files.internal("core/assets/fonts/score.fnt"));
        this.timer = random.nextFloat() * (Inimigo.SPAWN_MAX_TIME - Inimigo.SPAWN_MIN_TIME) + Inimigo.SPAWN_MIN_TIME;
        lifeBar = new Texture ("core/assets/blank.png");
        playerCollision = new Collision(0, 0, SHIP_WIDTH, SHIP_HEIGHT);

    }

    @Override
    public void dispose() {
        player.getTexture().dispose();
    }

    @Override
    public void render(float delta) {

        //Inimigo
        timer -= delta;
        if (timer <= 0){
            timer = random.nextFloat() * (Inimigo.SPAWN_MAX_TIME - Inimigo.SPAWN_MIN_TIME) + Inimigo.SPAWN_MIN_TIME;
            spawn.spawnInimigos();
       }


        for (SpaceObjects objeto: spawn.getObjetos()){
            objeto.update(delta);
            if (objeto.destroy) spawn.addObjetosRemovidos(objeto);
        }


        //Player
        player.tirosControl();
        player.shipControl(SPEED, delta);
        player.collisionControl();

        //TODO: encapsular abaixo.

        ArrayList<Tiro> removerTiros = new ArrayList<Tiro>(); //TODO: Tentar tirar essa arraylist secundaria e implentar o remove na classe.
        for (Tiro tiro : player.tiros){
            tiro.update(delta);
            if (tiro.isDestroy()) removerTiros.add(tiro);
        }


        playerCollision.move(player.getX(), player.getY());

        for (Tiro tiro: player.tiros) {
            for (SpaceObjects objeto: spawn.getObjetos()){
                if (tiro.getCollision().collidesWith(objeto.getCollision())){

                    if(objeto instanceof Inimigo) {
                        player.setScore(10);
                        spawn.addObjetosRemovidos(objeto);
                        removerTiros.add(tiro);
                    }
                    if (objeto instanceof SuperInimigo){

                        if(objeto.getLife() == 0){
                            spawn.addObjetosRemovidos(objeto);
                            player.setScore(30);

                        }
                        objeto.setLife();
                        removerTiros.add(tiro);

                    }

                }
            }
        }

        for (SpaceObjects objeto: spawn.getObjetos()) {
            if (objeto.getCollision().collidesWith(playerCollision)) {
                spawn.addObjetosRemovidos(objeto);
                if(objeto instanceof Inimigo){
                    player.setLife(objeto.hit());
                }

                if(objeto instanceof SuperInimigo){
                    player.setLife(objeto.hit());
                }
                if(objeto instanceof PowerLife){
                    player.setLife(objeto.hit());
                }


                if (player.getLife() <= 0) {
                    this.dispose();

                    game.setScreen(new GameOver(game, player.getScore()));


                }
            }
        }

        player.tiros.removeAll(removerTiros);




        spawn.getObjetos().removeAll(spawn.getObjetosRemovidos());

        //Game Batch
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);



        game.batch.begin();

         //Desenho do Sprite

        GlyphLayout scoreLayout = new GlyphLayout(scoreFont, "" + player.getScore());
        scoreFont.draw(game.batch, scoreLayout, Gdx.graphics.getWidth() / 2 - scoreLayout.width / 2, Gdx.graphics.getHeight() - scoreLayout.height - 10);

        for (Tiro tiro : player.tiros){
            tiro.render(game.batch);
        }

        for (SpaceObjects objeto: spawn.getObjetos()){
            objeto.render(game.batch);
        }

        if(player.getLife() >= 100) {
            game.batch.setColor(0,255,0, 1);
        }
        else if (player.getLife() > 40) {
            game.batch.setColor(255,165,0, 1);
        }
        else {
            game.batch.setColor( 255,0,0, 1);
        }

        game.batch.draw(lifeBar, 0, 0, Gdx.graphics.getWidth() * (player.getLife() * 0.01f) , 10);
        game.batch.setColor(255, 255, 255, 1);

        player.getSprite().draw(game.batch);
        player.getSprite().setPosition(player.getX(), player.getY());

        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }


}
