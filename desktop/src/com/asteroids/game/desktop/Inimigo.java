package com.asteroids.game.desktop;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;
import java.util.Random;

//TODO: considerar o uso de threads para inicialização.

public class Inimigo extends SpaceObjects{
    protected static Texture asteroid;

    public static final float SPAWN_MIN_TIME = 0.1f;
    public static final float SPAWN_MAX_TIME = 2.0f;

    Random random;


    private int flag;
    protected boolean show;
    private float maxTime;
    private float minTime;


    Collision collision;

    public boolean destroy = false;

    public Inimigo (float x, float y, int flag, float speed){
        super(x, y, flag, SPAWN_MAX_TIME, SPAWN_MIN_TIME, speed,"./core/assets/asteroid.png" );
        this.random = new Random();
        this.flag = flag;
        this.collision = new Collision(getX(), getY(), 50, 50);


    }

    @Override
    public int hit() {
        return this.damage += -10;
    }

    @Override
    public void render (SpriteBatch batch){ super.render(batch); }

}
