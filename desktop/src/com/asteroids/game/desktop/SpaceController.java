package com.asteroids.game.desktop;

public abstract class SpaceController {

    protected float x;
    protected float y;


    protected float speed;

    protected int width;
    protected int height;

    protected Collision collision;

    SpaceController (float x,float y, int width, int height, float speed){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.speed = speed;

    }


    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

}
