package com.asteroids.game.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.asteroids.game.Asteroids;

import java.awt.*;

public class DesktopLauncher {

	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.foregroundFPS = 60;
		config.title = "Asteroids";
		config.width = Asteroids.WIDTH_SCREEN;
		config.height = Asteroids.HEIGHT_SCREEN;
		config.fullscreen = false;
		config.vSyncEnabled = false;
		config.resizable = false;

		new LwjglApplication(new Asteroids(), config);
	}

	public void render (){

	}
}
