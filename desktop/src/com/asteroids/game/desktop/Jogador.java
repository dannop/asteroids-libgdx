package com.asteroids.game.desktop;

import com.asteroids.game.Asteroids;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

import java.awt.*;
import java.util.ArrayList;

import static com.badlogic.gdx.math.MathUtils.*;

//TODO: power up para aumentar a velocidade do jogador.

public class Jogador extends SpaceController  {

    private Texture ship;
    Asteroids game;
    private Sprite sprite;

    public ArrayList<Tiro> tiros;
    private float shipAnimation; //Variavel a ser usada quando houver uma animacao
    private int score;
    private float life;

    public Jogador (){
        super (Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, 60, 60, 200);
        this.ship = new Texture("./core/assets/Mario-Star-PNG-Image.png"); //Imagem da Nave
        this.sprite = new Sprite(ship);
        this.sprite.setSize(width,height);
        this.sprite.setOriginCenter();
        this.collision = new Collision(x, y, width, height);
        this.score = 0;
        this.tiros = new ArrayList<Tiro>();
        this.life = 100;
    }

    public void shipControl(float speed, float deltaTime){ //TODO: Movimentacao 360°. Não usar a tela maximizada.

        if ( Gdx.input.isKeyPressed(Input.Keys.A) && x >0){  //Limita o lado esquerdo.
            x -= speed * deltaTime;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.D) && (x + width < Gdx.graphics.getWidth()) ){
            x += speed * deltaTime;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.W) && (y + height < Gdx.graphics.getHeight())){
            y += speed * deltaTime;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.S) && y >= 0) {
            y -= speed * deltaTime;
        }

    }

    public void tirosControl(){
        Tiro.timer += Gdx.graphics.getDeltaTime();
        if (Gdx.input.isButtonPressed(Input.Keys.LEFT) && Tiro.timer >= Tiro.COOLDOWN){
            Tiro.timer = 0;
            tiros.add(new Tiro(x + (height / 2), y));
        }



    }


    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public void collisionControl (){
        collision.move(x, y);
    }

    public Texture getTexture() {
        return this.ship;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int point) {
        this.score = score + point;
    }

    public float getLife() {
        return life;
    }

    public void setLife(float value) {
        this.life = life + value;
    }

    public Collision getCollision() {
        return collision;
    }

    public Sprite getSprite() {
        return sprite;
    }
}
