package com.asteroids.game.desktop;

//Função de Colisão com Retangulos. Pega na internet.

public class Collision {

    float x, y;
    float width, height;

    public Collision (float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void move (float x, float y) {
        this.x = x;
        this.y = y;
    }

    public boolean collidesWith (Collision rect) {

        return x < rect.x + rect.width && y < rect.y + rect.height && x + width > rect.x && y + height > rect.y;
    }

}