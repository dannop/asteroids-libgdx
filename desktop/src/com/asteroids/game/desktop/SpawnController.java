package com.asteroids.game.desktop;

import com.badlogic.gdx.Gdx;

import java.util.ArrayList;
import java.util.Random;

public class SpawnController {
    Random random;

    public ArrayList<SpaceObjects> objetos;
    ArrayList<SpaceObjects> objetosRemovidos = new ArrayList<SpaceObjects>();

    int show;
    int opc;


    public SpawnController(){
        this.random = new Random();
        this.objetos = new ArrayList<>();

    }

    public void spawnInimigos(){

        show = random.nextInt(3);
        opc = random.nextInt(21);


        if (show  == 0){
            if (opc == 0){
                this.objetos.add(new PowerLife(random.nextInt(Gdx.graphics.getWidth() - 60), Gdx.graphics.getHeight(), show));
            }
            if (opc > 1 && opc < 4){
                this.objetos.add(new SuperInimigo(random.nextInt(Gdx.graphics.getWidth() - 60), Gdx.graphics.getHeight(), show));
            }

            else {
                this.objetos.add(new Inimigo(random.nextInt(Gdx.graphics.getWidth() - 60), Gdx.graphics.getHeight(), show, 200));
            }
        }

        if (show == 1){
            if (opc == 0){
                this.objetos.add(new PowerLife(random.nextInt(Gdx.graphics.getWidth() - 60), 0, show));
            }
            if (opc > 1 && opc < 3){
                this.objetos.add(new SuperInimigo(random.nextInt(Gdx.graphics.getWidth() - 60), 0, show));
            }

            else {
                this.objetos.add(new Inimigo(random.nextInt(Gdx.graphics.getWidth() - 60), 0, show, 200));
            }
        }

        if (show == 2) {
            if (opc == 0) {
                this.objetos.add(new PowerLife(Gdx.graphics.getWidth(), (random.nextInt(Gdx.graphics.getHeight() - 60)), show));
            }
            if (opc > 1 && opc < 3){
                this.objetos.add(new SuperInimigo(Gdx.graphics.getWidth(), (random.nextInt(Gdx.graphics.getHeight() - 60)), show));
            }

            else {
                this.objetos.add(new Inimigo(Gdx.graphics.getWidth(), (random.nextInt(Gdx.graphics.getHeight() - 60)), show, 200));
            }
        }

        if (show == 3) {
            if (opc == 0) {
                this.objetos.add(new PowerLife(0, (random.nextInt(Gdx.graphics.getHeight() - 60)), show));
            }
            if (opc > 1 && opc < 3){
                this.objetos.add(new SuperInimigo(0, (random.nextInt(Gdx.graphics.getHeight() - 60)), show));
            }
            else {
                this.objetos.add(new Inimigo(0, (random.nextInt(Gdx.graphics.getHeight() - 60)), show, 200));
            }
        }
    }


    public ArrayList<SpaceObjects> getObjetos() {
        return objetos;
    }

    public void setObjetos(ArrayList<SpaceObjects> objetos) {
        this.objetos = objetos;
    }

    public ArrayList<SpaceObjects> getObjetosRemovidos() {
        return objetosRemovidos;
    }

    public void addObjetosRemovidos(SpaceObjects objeto) {
        this.objetosRemovidos.add(objeto);
    }
}
