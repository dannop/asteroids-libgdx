package com.asteroids.game.desktop;

public class Position {
    public double x;
    public double y;
    private double catAdj;
    private double catOpt;
    private double hip;

    public Position(float x, float y){
        this.x = x;
        this.y = y;
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }


    public void setCatAdj(Position finalPos){
        this.catAdj = x - finalPos.x;
    }

    public void setCatOpt(Position finalPos){
        this.catOpt = y - finalPos.y;
    }

    public void setHip(){
        this.hip =  Math.sqrt((Math.pow(catOpt,2) + Math.pow(catAdj,2)));
    }

    public double getSen(){
        return catOpt/hip;
    }

    public double getCos(){
        return catAdj/hip;
    }
}
