package com.asteroids.game.desktop;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PowerLife extends SpaceObjects{


    public PowerLife (float x, float y, int flag) {

        super(x, y, flag, 5, 3, 300, "core/assets/600px-Logo_Yandex_health.png");
    }

    @Override
    public void render (SpriteBatch batch){ super.render(batch); }

    @Override
    public int hit() {
        return this.damage += 10;
    }



}
