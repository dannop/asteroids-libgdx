package com.asteroids.game.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Random;

public abstract class SpaceObjects extends SpaceController   {

    private Texture image;

    Random random;
    protected int damage;
    public static float spawnMinTime;
    public static float spawnMaxTime;
    protected int flag;
    protected int life;


    Collision collision;

    public boolean destroy = false;

    public SpaceObjects (float x, float y, int flag, float maxTime, float minTime, float speed, String path){

        super(x, y, 50, 50, speed);
        this.random = new Random();
        this.spawnMinTime = minTime;
        this.spawnMaxTime = maxTime;
        this.flag = flag;
        this.collision = new Collision(getX(), getY(), 50, 50);

        if (this.image == null){
            this.image = new Texture(path);
        }
    }


    public void update (float deltaTime) {

        if (flag == 0) {

            y -= speed * deltaTime;


            if (y + height < 0) {

                destroy = true;
            }
        }
        if (flag == 1) {

            y += speed * deltaTime;

            if (y + height > Gdx.graphics.getHeight()) {
                destroy = true;
            }
        }

        if (flag == 2) {

            x -= speed * deltaTime;

            if (x + width < 0) {

                destroy = true;
            }
        }

        if (flag == 3) {

            x += speed * deltaTime;

            if (x + width > Gdx.graphics.getWidth()) {
                destroy = true;
            }
        }

        collision.move(x, y);
    }


    public void render (SpriteBatch batch){ batch.draw(this.image, x, y, width, height); }

    public Collision getCollision() {
        return collision;
    }


    public int hit() {
       return this.damage += damage;
    }

    public Texture getImage() {
        return this.image;
    }

    public int getLife() {
        return life;
    }

    public void setLife() {
        this.life -= 1;
    }
}