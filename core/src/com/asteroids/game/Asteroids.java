package com.asteroids.game;

import com.asteroids.game.desktop.GameController;
import com.asteroids.game.desktop.GameOver;
import com.asteroids.game.desktop.Menu;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class Asteroids extends Game {

	public static final int WIDTH_SCREEN = 960;
	public static final int HEIGHT_SCREEN = 540;
	public SpriteBatch batch;


    @Override
	public void create () {

		batch = new SpriteBatch();
		this.setScreen(new GameController(this));
		this.setScreen(new Menu(this));

	}

	@Override
	public void render () {

    	super.render(); //Usa o mesmo render que o GameController


	}

}
